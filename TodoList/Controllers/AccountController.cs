﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TodoListApp.ControllerModels.AccountController;
using TodoListApp.DbContexts;
using TodoListApp.Models;

namespace TodoListApp.Controllers
{
    [Route("/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private UserManager<User> _userManager;
        private IConfiguration _config;
        public AccountController(UserManager<User> uManager, IConfiguration configuration)
        {
            _userManager = uManager;
            _config = configuration;
        }
        [Route("/login")]
        [HttpPost]
        public IActionResult LoginAccount(Login bodyLogin)
        {
            try
            {
                var userListWithTodo = _userManager.Users.Include(u => u.Todo);
                var existUser = userListWithTodo.SingleOrDefaultAsync(u => u.UserName == bodyLogin.Username).Result;

                if(existUser == null)
                {
                    return BadRequest("Incorrect Username or Password");
                }

                bool isPasswordMatch = _userManager.CheckPasswordAsync(existUser, bodyLogin.Password).Result;

                if (isPasswordMatch)
                {
                    return Ok(GenerateJwtToken(existUser));
                }
                else
                {
                    return BadRequest("Incorrect Username or Password");
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Route("/register")]
        [HttpPost]
        public IActionResult CreateAccount(Register bodyRegister)
        {
            try
            {
                var checkUserExist = _userManager.FindByNameAsync(bodyRegister.Username).Result;
                if (checkUserExist != null)
                {
                    return BadRequest("Username already exists");
                }

                var newUser = new User
                {
                    UserName = bodyRegister.Username,
                    Email = bodyRegister.Email,
                    Todo = new TodoList()
                };

                var result = _userManager.CreateAsync(newUser, bodyRegister.Password).Result;
                if (result.Succeeded)
                {
                    return Ok(GenerateJwtToken(newUser));
                }

                return BadRequest(result.Errors.First().Description);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private string GenerateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.Unicode.GetBytes(_config["JwtToken:Secret"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(_userManager.Options.ClaimsIdentity.UserIdClaimType, user.Id.ToString())
                }),
                Issuer = _config["JwtToken:Issuer"],
                Audience = _config["JwtToken:Audience"],
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
