﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListApp.ControllerModels.TodoListController;
using TodoListApp.DbContexts;
using TodoListApp.Models;

namespace TodoListApp.Controllers
{
    [Route("/")]
    [ApiController]
    [Authorize]
    public class TodoListController : ControllerBase
    {
        private UserManager<User> _userManager;
        private IConfiguration _config;
        public TodoListController(UserManager<User> uManager, IConfiguration configuration)
        {
            _userManager = uManager;
            _config = configuration;
        }
        [HttpGet]
        public IActionResult GetTodoDetail()
        {
            try
            {
                var userId = User.Claims.First(c => c.Type == _userManager.Options.ClaimsIdentity.UserIdClaimType).Value;
                var user = _userManager.Users.Include(u => u.Todo).ThenInclude(tdl => tdl.DetailCollection).SingleAsync(u => u.Id.ToString() == userId).Result;
                
                if (user.Todo.DetailCollection == null)
                {                  
                    return Ok();
                }
                var detailList = user.Todo.DetailCollection.Select(d => new { d.Id, d.IsDone, d.Content });
                return Ok(detailList.Reverse());
            }
            catch(Exception e)
            {
                throw new Exception($"TodoList GetTodoDetail: {e.Message}");
            }
        }
        [HttpPost]
        public IActionResult InsertNewTodoDetail(NewTodoDetail bodyData)
        {
            try
            {
                var userId = User.Claims.First(c => c.Type == _userManager.Options.ClaimsIdentity.UserIdClaimType).Value;
                var user = _userManager.Users.Include(u => u.Todo).ThenInclude(tdl => tdl.DetailCollection).SingleAsync(u => u.Id.ToString() == userId).Result;
                
                if(user.Todo.DetailCollection == null)
                {
                    user.Todo.DetailCollection = new List<TodoListDetail>();                 
                }

                if(user.Todo.DetailCollection.Count > 20)
                {
                    return BadRequest("TodoList can only carries upto 20 rows");
                }

                var newTodoDetail = new TodoListDetail
                {
                    IsDone = false,
                    Content = bodyData.Content
                };
                user.Todo.DetailCollection.Add(newTodoDetail);

                var result = _userManager.UpdateAsync(user).Result;
                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors.First());
                }
                return Ok(newTodoDetail.Id);
            }
            catch(Exception e)
            {
                throw new Exception($"TodoList InsertNewTodoDetail: {e.Message}");
            }
        }
        [HttpPut]
        public IActionResult UpdateTodoDetail(UpdateTodoDetail bodyData)
        {
            try
            {
                if (bodyData.Id == Guid.Empty)
                {
                    return BadRequest("Id not found");
                }

                var userId = User.Claims.First(c => c.Type == _userManager.Options.ClaimsIdentity.UserIdClaimType).Value;
                var user = _userManager.Users.Include(u => u.Todo).ThenInclude(tdl => tdl.DetailCollection).SingleAsync(u => u.Id.ToString() == userId).Result;

                var detailItem = user.Todo.DetailCollection.Where(d => d.Id == bodyData.Id).SingleOrDefault();
                if (detailItem == null)
                {
                    return BadRequest("Detail Id not found");
                }

                detailItem.Content = bodyData.Content;
                detailItem.IsDone = bodyData.IsDone;

                var updateResult = _userManager.UpdateAsync(user).Result;
                if (!updateResult.Succeeded)
                {
                    throw new Exception(updateResult.Errors.SingleOrDefault().Description);
                }

                return Ok("Success");
            }
            catch(Exception e)
            {
                throw new Exception($"TodoList UpdateTodoDetail: {e.Message}");
            }
        }
        [Route("/{id?}")]
        [HttpDelete]
        public IActionResult DeleteTodoDetail(Guid? id)
        {
            try
            {
                var userId = User.Claims.First(c => c.Type == _userManager.Options.ClaimsIdentity.UserIdClaimType).Value;
                var user = _userManager.Users.Include(u => u.Todo).ThenInclude(tdl => tdl.DetailCollection).SingleAsync(u => u.Id.ToString() == userId).Result;
                
                if(user.Todo.DetailCollection == null)
                {
                    return BadRequest("Id not found");
                }

                var detailItem = user.Todo.DetailCollection.SingleOrDefault(d => d.Id == id);
                if(detailItem == null)
                {
                    return BadRequest("Id not found");
                }

                user.Todo.DetailCollection.Remove(detailItem);

                var updateResult = _userManager.UpdateAsync(user).Result;
                if (!updateResult.Succeeded)
                {
                    throw new Exception(updateResult.Errors.First().Description);
                }

                return Ok("Successs");
            }
            catch (Exception e)
            {
                throw new Exception($"TodoList DeleteTodoDetail: {e.Message}");
            }
        }
        [Route("/test")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Testing()
        {
            return Ok($"Hello");
        }
    }
}
