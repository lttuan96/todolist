﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListApp.Models;

namespace TodoListApp.DbContexts
{
    public class TodoListDbContext: DbContext
    {
        public DbSet<TodoList> ToDoList { get; set; }
        public DbSet<TodoListDetail> DetailList { get; set; }
        public DbSet<User> UserList { get; set; }
        public TodoListDbContext(DbContextOptions<TodoListDbContext> options): base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id);
            modelBuilder.Entity<User>()
                .HasIndex(u => u.UserName)
                .IsUnique();
            modelBuilder.Entity<User>()
                .HasOne(u => u.Todo)
                .WithOne(tdl => tdl.User)
                .HasForeignKey<TodoList>(tdl => tdl.UserId);
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<TodoList>()
                .Property(tdl => tdl.UserId)
                .IsRequired();
            modelBuilder.Entity<TodoList>()
                .HasIndex(tdl => tdl.UserId);
            modelBuilder.Entity<TodoList>()
                .HasMany(tdl => tdl.DetailCollection)
                .WithOne(d => d.Todo)
                .HasForeignKey(d => d.TodoId);
            modelBuilder.Entity<TodoList>()
                .HasOne(tdl => tdl.User)
                .WithOne(u => u.Todo);

            modelBuilder.Entity<TodoListDetail>()
                .HasKey(d => d.Id);
            modelBuilder.Entity<TodoListDetail>()
                .Property(d => d.IsDone)
                .IsRequired();
            modelBuilder.Entity<TodoListDetail>()
                .Property(d => d.Content)
                .IsRequired();
            modelBuilder.Entity<TodoListDetail>()
                .Property(d => d.TodoId)
                .IsRequired();
            modelBuilder.Entity<TodoListDetail>()
                .HasOne(d => d.Todo)
                .WithMany(tdl => tdl.DetailCollection)
                .IsRequired();
        }

    }
}
