﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoListApp.Models
{
    public class TodoListDetail
    {
        public Guid Id { get; set; }
        public Guid TodoId { get; set; }
        public bool IsDone { get; set; }
        public string Content { get; set; }
        public TodoList Todo { get; set; }
    }
}
