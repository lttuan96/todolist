﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TodoListApp.Models
{
    public class User: IdentityUser<Guid>
    {
        public TodoList Todo { get; set; }
    }
}
