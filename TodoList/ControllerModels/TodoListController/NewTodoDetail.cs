﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TodoListApp.ControllerModels.TodoListController
{
    public class NewTodoDetail
    {
        [Required]
        public string Content { get; set; }
    }
}
