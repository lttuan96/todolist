﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TodoListApp.ControllerModels.TodoListController
{
    public class UpdateTodoDetail
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public bool IsDone { get; set; }
    }
}
